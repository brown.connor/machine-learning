# machine-learning

This project contains a range of materials related to Machine Learning and Deep 
Learning.

## Learning materials
I have written a [paper](notes/an_introduction_to_machine_learning.pdf) that 
highlights the main areas, algorithms and techniques in machine learning and 
deep learning. The writing of this paper was aided greatly by Dr Andrew Ng's 
courses on [Machine Learning](https://www.coursera.org/learn/machine-learning) 
and [Deep Learning](https://www.coursera.org/specializations/deep-learning?).

## TensorFlow
A range of examples and mini-projects can be found in the 
[TensorFlow](tensorflow) directory.